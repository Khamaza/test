/* Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * MIT Licensed.
 */
// Inspired by base2 and Prototype
(function(){
    var initializing = false;

    // The base JQClass implementation (does nothing)
    window.JQClass = function(){};

    // Collection of derived classes
    JQClass.classes = {};

    // Create a new JQClass that inherits from this class
    JQClass.extend = function extender(prop) {
        var base = this.prototype;

        // Instantiate a base class (but only create the instance,
        // don't run the init constructor)
        initializing = true;
        var prototype = new this();
        initializing = false;

        // Copy the properties over onto the new prototype
        for (var name in prop) {
            // Check if we're overwriting an existing function
            prototype[name] = typeof prop[name] == 'function' &&
            typeof base[name] == 'function' ?
                (function(name, fn){
                    return function() {
                        var __super = this._super;

                        // Add a new ._super() method that is the same method
                        // but on the super-class
                        this._super = function(args) {
                            return base[name].apply(this, args || []);
                        };

                        var ret = fn.apply(this, arguments);

                        // The method only need to be bound temporarily, so we
                        // remove it when we're done executing
                        this._super = __super;

                        return ret;
                    };
                })(name, prop[name]) :
                prop[name];
        }

        // The dummy class constructor
        function JQClass() {
            // All construction is actually done in the init method
            if (!initializing && this._init) {
                this._init.apply(this, arguments);
            }
        }

        // Populate our constructed prototype object
        JQClass.prototype = prototype;

        // Enforce the constructor to be what we expect
        JQClass.prototype.constructor = JQClass;

        // And make this class extendable
        JQClass.extend = extender;

        return JQClass;
    };
})();

(function($) { // Ensure $, encapsulate

    /** Abstract base class for collection plugins v1.0.1.
     Written by Keith Wood (kbwood{at}iinet.com.au) December 2013.
     Licensed under the MIT (http://keith-wood.name/licence.html) license.
     @module $.JQPlugin
     @abstract */
    JQClass.classes.JQPlugin = JQClass.extend({

        /** Name to identify this plugin.
         @example name: 'tabs' */
        name: 'plugin',

        /** Default options for instances of this plugin (default: {}).
         @example defaultOptions: {
 	selectedClass: 'selected',
 	triggers: 'click'
 } */
        defaultOptions: {},

        /** Options dependent on the locale.
         Indexed by language and (optional) country code, with '' denoting the default language (English/US).
         @example regionalOptions: {
	'': {
		greeting: 'Hi'
	}
 } */
        regionalOptions: {},

        /** Names of getter methods - those that can't be chained (default: []).
         @example _getters: ['activeTab'] */
        _getters: [],

        /** Retrieve a marker class for affected elements.
         @private
         @return {string} The marker class. */
        _getMarker: function() {
            return 'is-' + this.name;
        },

        /** Initialise the plugin.
         Create the jQuery bridge - plugin name <code>xyz</code>
         produces <code>$.xyz</code> and <code>$.fn.xyz</code>. */
        _init: function() {
            // Apply default localisations
            $.extend(this.defaultOptions, (this.regionalOptions && this.regionalOptions['']) || {});
            // Camel-case the name
            var jqName = camelCase(this.name);
            // Expose jQuery singleton manager
            $[jqName] = this;
            // Expose jQuery collection plugin
            $.fn[jqName] = function(options) {
                var otherArgs = Array.prototype.slice.call(arguments, 1);
                if ($[jqName]._isNotChained(options, otherArgs)) {
                    return $[jqName][options].apply($[jqName], [this[0]].concat(otherArgs));
                }
                return this.each(function() {
                    if (typeof options === 'string') {
                        if (options[0] === '_' || !$[jqName][options]) {
                            throw 'Unknown method: ' + options;
                        }
                        $[jqName][options].apply($[jqName], [this].concat(otherArgs));
                    }
                    else {
                        $[jqName]._attach(this, options);
                    }
                });
            };
        },

        /** Set default values for all subsequent instances.
         @param options {object} The new default options.
         @example $.plugin.setDefauls({name: value}) */
        setDefaults: function(options) {
            $.extend(this.defaultOptions, options || {});
        },

        /** Determine whether a method is a getter and doesn't permit chaining.
         @private
         @param name {string} The method name.
         @param otherArgs {any[]} Any other arguments for the method.
         @return {boolean} True if this method is a getter, false otherwise. */
        _isNotChained: function(name, otherArgs) {
            if (name === 'option' && (otherArgs.length === 0 ||
                (otherArgs.length === 1 && typeof otherArgs[0] === 'string'))) {
                return true;
            }
            return $.inArray(name, this._getters) > -1;
        },

        /** Initialise an element. Called internally only.
         Adds an instance object as data named for the plugin.
         @param elem {Element} The element to enhance.
         @param options {object} Overriding settings. */
        _attach: function(elem, options) {
            elem = $(elem);
            if (elem.hasClass(this._getMarker())) {
                return;
            }
            elem.addClass(this._getMarker());
            options = $.extend({}, this.defaultOptions, this._getMetadata(elem), options || {});
            var inst = $.extend({name: this.name, elem: elem, options: options},
                this._instSettings(elem, options));
            elem.data(this.name, inst); // Save instance against element
            this._postAttach(elem, inst);
            this.option(elem, options);
        },

        /** Retrieve additional instance settings.
         Override this in a sub-class to provide extra settings.
         @param elem {jQuery} The current jQuery element.
         @param options {object} The instance options.
         @return {object} Any extra instance values.
         @example _instSettings: function(elem, options) {
 	return {nav: elem.find(options.navSelector)};
 } */
        _instSettings: function(elem, options) {
            return {};
        },

        /** Plugin specific post initialisation.
         Override this in a sub-class to perform extra activities.
         @param elem {jQuery} The current jQuery element.
         @param inst {object} The instance settings.
         @example _postAttach: function(elem, inst) {
 	elem.on('click.' + this.name, function() {
 		...
 	});
 } */
        _postAttach: function(elem, inst) {
        },

        /** Retrieve metadata configuration from the element.
         Metadata is specified as an attribute:
         <code>data-&lt;plugin name>="&lt;setting name>: '&lt;value>', ..."</code>.
         Dates should be specified as strings in this format: 'new Date(y, m-1, d)'.
         @private
         @param elem {jQuery} The source element.
         @return {object} The inline configuration or {}. */
        _getMetadata: function(elem) {
            try {
                var data = elem.data(this.name.toLowerCase()) || '';
                data = data.replace(/'/g, '"');
                data = data.replace(/([a-zA-Z0-9]+):/g, function(match, group, i) {
                    var count = data.substring(0, i).match(/"/g); // Handle embedded ':'
                    return (!count || count.length % 2 === 0 ? '"' + group + '":' : group + ':');
                });
                data = $.parseJSON('{' + data + '}');
                for (var name in data) { // Convert dates
                    var value = data[name];
                    if (typeof value === 'string' && value.match(/^new Date\((.*)\)$/)) {
                        data[name] = eval(value);
                    }
                }
                return data;
            }
            catch (e) {
                return {};
            }
        },

        /** Retrieve the instance data for element.
         @param elem {Element} The source element.
         @return {object} The instance data or {}. */
        _getInst: function(elem) {
            return $(elem).data(this.name) || {};
        },

        /** Retrieve or reconfigure the settings for a plugin.
         @param elem {Element} The source element.
         @param name {object|string} The collection of new option values or the name of a single option.
         @param [value] {any} The value for a single named option.
         @return {any|object} If retrieving a single value or all options.
         @example $(selector).plugin('option', 'name', value)
         $(selector).plugin('option', {name: value, ...})
         var value = $(selector).plugin('option', 'name')
         var options = $(selector).plugin('option') */
        option: function(elem, name, value) {
            elem = $(elem);
            var inst = elem.data(this.name);
            if  (!name || (typeof name === 'string' && value == null)) {
                var options = (inst || {}).options;
                return (options && name ? options[name] : options);
            }
            if (!elem.hasClass(this._getMarker())) {
                return;
            }
            var options = name || {};
            if (typeof name === 'string') {
                options = {};
                options[name] = value;
            }
            this._optionsChanged(elem, inst, options);
            $.extend(inst.options, options);
        },

        /** Plugin specific options processing.
         Old value available in <code>inst.options[name]</code>, new value in <code>options[name]</code>.
         Override this in a sub-class to perform extra activities.
         @param elem {jQuery} The current jQuery element.
         @param inst {object} The instance settings.
         @param options {object} The new options.
         @example _optionsChanged: function(elem, inst, options) {
 	if (options.name != inst.options.name) {
 		elem.removeClass(inst.options.name).addClass(options.name);
 	}
 } */
        _optionsChanged: function(elem, inst, options) {
        },

        /** Remove all trace of the plugin.
         Override <code>_preDestroy</code> for plugin-specific processing.
         @param elem {Element} The source element.
         @example $(selector).plugin('destroy') */
        destroy: function(elem) {
            elem = $(elem);
            if (!elem.hasClass(this._getMarker())) {
                return;
            }
            this._preDestroy(elem, this._getInst(elem));
            elem.removeData(this.name).removeClass(this._getMarker());
        },

        /** Plugin specific pre destruction.
         Override this in a sub-class to perform extra activities and undo everything that was
         done in the <code>_postAttach</code> or <code>_optionsChanged</code> functions.
         @param elem {jQuery} The current jQuery element.
         @param inst {object} The instance settings.
         @example _preDestroy: function(elem, inst) {
 	elem.off('.' + this.name);
 } */
        _preDestroy: function(elem, inst) {
        }
    });

    /** Convert names from hyphenated to camel-case.
     @private
     @param value {string} The original hyphenated name.
     @return {string} The camel-case version. */
    function camelCase(name) {
        return name.replace(/-([a-z])/g, function(match, group) {
            return group.toUpperCase();
        });
    }

    /** Expose the plugin base.
     @namespace "$.JQPlugin" */
    $.JQPlugin = {

        /** Create a new collection plugin.
         @memberof "$.JQPlugin"
         @param [superClass='JQPlugin'] {string} The name of the parent class to inherit from.
         @param overrides {object} The property/function overrides for the new class.
         @example $.JQPlugin.createPlugin({
 	name: 'tabs',
 	defaultOptions: {selectedClass: 'selected'},
 	_initSettings: function(elem, options) { return {...}; },
 	_postAttach: function(elem, inst) { ... }
 }); */
        createPlugin: function(superClass, overrides) {
            if (typeof superClass === 'object') {
                overrides = superClass;
                superClass = 'JQPlugin';
            }
            superClass = camelCase(superClass);
            var className = camelCase(overrides.name);
            JQClass.classes[className] = JQClass.classes[superClass].extend(overrides);
            new JQClass.classes[className]();
        }
    };

})(jQuery);/**
 * Created by Serjok on 22.01.2016.
 */

/* http://keith-wood.name/countdown.html
   Countdown for jQuery v2.0.2.
   Written by Keith Wood (kbwood{at}iinet.com.au) January 2008.
   Available under the MIT (http://keith-wood.name/licence.html) license. 
   Please attribute the author if you use it. */
(function($) {
    var w = 'countdown';
    var Y = 0;
    var O = 1;
    var W = 2;
    var D = 3;
    var H = 4;
    var M = 5;
    var S = 6;
    $.JQPlugin.createPlugin({ name: w, defaultOptions: { until: null, since: null, timezone: null, serverSync: null, format: 'dHMS', layout: '', compact: false, padZeroes: false, significant: 0, description: '', expiryUrl: '', expiryText: '', alwaysExpire: false, onExpiry: null, onTick: null, tickInterval: 1 }, regionalOptions: { '': { labels: ['Years', 'Months', 'Weeks', 'Days', 'Hours', 'Minutes', 'Seconds'], labels1: ['Year', 'Month', 'Week', 'Day', 'Hour', 'Minute', 'Second'], compactLabels: ['y', 'm', 'w', 'd'], whichLabels: null, digits: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'], timeSeparator: ':', isRTL: false } }, _getters: ['getTimes'], _rtlClass: w + '-rtl', _sectionClass: w + '-section', _amountClass: w + '-amount', _periodClass: w + '-period', _rowClass: w + '-row', _holdingClass: w + '-holding', _showClass: w + '-show', _descrClass: w + '-descr', _timerElems: [], _init: function() {
            var c = this;
            this._super();
            this._serverSyncs = [];
            var d = (typeof Date.now == 'function' ? Date.now : function() {
                return new Date().getTime() });
            var e = (window.performance && typeof window.performance.now == 'function');

            function timerCallBack(a) {
                var b = (a < 1e12 ? (e ? (performance.now() + performance.timing.navigationStart) : d()) : a || d());
                if (b - g >= 1000) { c._updateElems();
                    g = b }
                f(timerCallBack) }
            var f = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || null;
            var g = 0;
            if (!f || $.noRequestAnimationFrame) { $.noRequestAnimationFrame = null;
                setInterval(function() { c._updateElems() }, 980) } else { g = window.animationStartTime || window.webkitAnimationStartTime || window.mozAnimationStartTime || window.oAnimationStartTime || window.msAnimationStartTime || d();
                f(timerCallBack) } }, UTCDate: function(a, b, c, e, f, g, h, i) {
            if (typeof b == 'object' && b.constructor == Date) { i = b.getMilliseconds();
                h = b.getSeconds();
                g = b.getMinutes();
                f = b.getHours();
                e = b.getDate();
                c = b.getMonth();
                b = b.getFullYear() }
            var d = new Date();
            d.setUTCFullYear(b);
            d.setUTCDate(1);
            d.setUTCMonth(c || 0);
            d.setUTCDate(e || 1);
            d.setUTCHours(f || 0);
            d.setUTCMinutes((g || 0) - (Math.abs(a) < 30 ? a * 60 : a));
            d.setUTCSeconds(h || 0);
            d.setUTCMilliseconds(i || 0);
            return d }, periodsToSeconds: function(a) {
            return a[0] * 31557600 + a[1] * 2629800 + a[2] * 604800 + a[3] * 86400 + a[4] * 3600 + a[5] * 60 + a[6] }, resync: function() {
            var d = this;
            $('.' + this._getMarker()).each(function() {
                var a = $.data(this, d.name);
                if (a.options.serverSync) {
                    var b = null;
                    for (var i = 0; i < d._serverSyncs.length; i++) {
                        if (d._serverSyncs[i][0] == a.options.serverSync) { b = d._serverSyncs[i];
                            break } }
                    if (b[2] == null) {
                        var c = ($.isFunction(a.options.serverSync) ? a.options.serverSync.apply(this, []) : null);
                        b[2] = (c ? new Date().getTime() - c.getTime() : 0) - b[1] }
                    if (a._since) { a._since.setMilliseconds(a._since.getMilliseconds() + b[2]) }
                    a._until.setMilliseconds(a._until.getMilliseconds() + b[2]) } });
            for (var i = 0; i < d._serverSyncs.length; i++) {
                if (d._serverSyncs[i][2] != null) { d._serverSyncs[i][1] += d._serverSyncs[i][2];
                    delete d._serverSyncs[i][2] } } }, _instSettings: function(a, b) {
            return { _periods: [0, 0, 0, 0, 0, 0, 0] } }, _addElem: function(a) {
            if (!this._hasElem(a)) { this._timerElems.push(a) } }, _hasElem: function(a) {
            return ($.inArray(a, this._timerElems) > -1) }, _removeElem: function(b) { this._timerElems = $.map(this._timerElems, function(a) {
                return (a == b ? null : a) }) }, _updateElems: function() {
            for (var i = this._timerElems.length - 1; i >= 0; i--) { this._updateCountdown(this._timerElems[i]) } }, _optionsChanged: function(a, b, c) {
            if (c.layout) { c.layout = c.layout.replace(/&lt;/g, '<').replace(/&gt;/g, '>') }
            this._resetExtraLabels(b.options, c);
            var d = (b.options.timezone != c.timezone);
            $.extend(b.options, c);
            this._adjustSettings(a, b, c.until != null || c.since != null || d);
            var e = new Date();
            if ((b._since && b._since < e) || (b._until && b._until > e)) { this._addElem(a[0]) }
            this._updateCountdown(a, b) }, _updateCountdown: function(a, b) { a = a.jquery ? a : $(a);
            b = b || this._getInst(a);
            if (!b) {
                return }
            a.html(this._generateHTML(b)).toggleClass(this._rtlClass, b.options.isRTL);
            if ($.isFunction(b.options.onTick)) {
                var c = b._hold != 'lap' ? b._periods : this._calculatePeriods(b, b._show, b.options.significant, new Date());
                if (b.options.tickInterval == 1 || this.periodsToSeconds(c) % b.options.tickInterval == 0) { b.options.onTick.apply(a[0], [c]) } }
            var d = b._hold != 'pause' && (b._since ? b._now.getTime() < b._since.getTime() : b._now.getTime() >= b._until.getTime());
            if (d && !b._expiring) { b._expiring = true;
                if (this._hasElem(a[0]) || b.options.alwaysExpire) { this._removeElem(a[0]);
                    if ($.isFunction(b.options.onExpiry)) { b.options.onExpiry.apply(a[0], []) }
                    if (b.options.expiryText) {
                        var e = b.options.layout;
                        b.options.layout = b.options.expiryText;
                        this._updateCountdown(a[0], b);
                        b.options.layout = e }
                    if (b.options.expiryUrl) { window.location = b.options.expiryUrl } }
                b._expiring = false } else if (b._hold == 'pause') { this._removeElem(a[0]) } }, _resetExtraLabels: function(a, b) {
            for (var n in b) {
                if (n.match(/[Ll]abels[02-9]|compactLabels1/)) { a[n] = b[n] } }
            for (var n in a) {
                if (n.match(/[Ll]abels[02-9]|compactLabels1/) && typeof b[n] === 'undefined') { a[n] = null } } }, _adjustSettings: function(a, b, c) {
            var d = null;
            for (var i = 0; i < this._serverSyncs.length; i++) {
                if (this._serverSyncs[i][0] == b.options.serverSync) { d = this._serverSyncs[i][1];
                    break } }
            if (d != null) {
                var e = (b.options.serverSync ? d : 0);
                var f = new Date() } else {
                var g = ($.isFunction(b.options.serverSync) ? b.options.serverSync.apply(a[0], []) : null);
                var f = new Date();
                var e = (g ? f.getTime() - g.getTime() : 0);
                this._serverSyncs.push([b.options.serverSync, e]) }
            var h = b.options.timezone;
            h = (h == null ? -f.getTimezoneOffset() : h);
            if (c || (!c && b._until == null && b._since == null)) { b._since = b.options.since;
                if (b._since != null) { b._since = this.UTCDate(h, this._determineTime(b._since, null));
                    if (b._since && e) { b._since.setMilliseconds(b._since.getMilliseconds() + e) } }
                b._until = this.UTCDate(h, this._determineTime(b.options.until, f));
                if (e) { b._until.setMilliseconds(b._until.getMilliseconds() + e) } }
            b._show = this._determineShow(b) }, _preDestroy: function(a, b) { this._removeElem(a[0]);
            a.empty() }, pause: function(a) { this._hold(a, 'pause') }, lap: function(a) { this._hold(a, 'lap') }, resume: function(a) { this._hold(a, null) }, toggle: function(a) {
            var b = $.data(a, this.name) || {};
            this[!b._hold ? 'pause' : 'resume'](a) }, toggleLap: function(a) {
            var b = $.data(a, this.name) || {};
            this[!b._hold ? 'lap' : 'resume'](a) }, _hold: function(a, b) {
            var c = $.data(a, this.name);
            if (c) {
                if (c._hold == 'pause' && !b) { c._periods = c._savePeriods;
                    var d = (c._since ? '-' : '+');
                    c[c._since ? '_since' : '_until'] = this._determineTime(d + c._periods[0] + 'y' + d + c._periods[1] + 'o' + d + c._periods[2] + 'w' + d + c._periods[3] + 'd' + d + c._periods[4] + 'h' + d + c._periods[5] + 'm' + d + c._periods[6] + 's');
                    this._addElem(a) }
                c._hold = b;
                c._savePeriods = (b == 'pause' ? c._periods : null);
                $.data(a, this.name, c);
                this._updateCountdown(a, c) } }, getTimes: function(a) {
            var b = $.data(a, this.name);
            return (!b ? null : (b._hold == 'pause' ? b._savePeriods : (!b._hold ? b._periods : this._calculatePeriods(b, b._show, b.options.significant, new Date())))) }, _determineTime: function(k, l) {
            var m = this;
            var n = function(a) {
                var b = new Date();
                b.setTime(b.getTime() + a * 1000);
                return b };
            var o = function(a) { a = a.toLowerCase();
                var b = new Date();
                var c = b.getFullYear();
                var d = b.getMonth();
                var e = b.getDate();
                var f = b.getHours();
                var g = b.getMinutes();
                var h = b.getSeconds();
                var i = /([+-]?[0-9]+)\s*(s|m|h|d|w|o|y)?/g;
                var j = i.exec(a);
                while (j) {
                    switch (j[2] || 's') {
                        case 's':
                            h += parseInt(j[1], 10);
                            break;
                        case 'm':
                            g += parseInt(j[1], 10);
                            break;
                        case 'h':
                            f += parseInt(j[1], 10);
                            break;
                        case 'd':
                            e += parseInt(j[1], 10);
                            break;
                        case 'w':
                            e += parseInt(j[1], 10) * 7;
                            break;
                        case 'o':
                            d += parseInt(j[1], 10);
                            e = Math.min(e, m._getDaysInMonth(c, d));
                            break;
                        case 'y':
                            c += parseInt(j[1], 10);
                            e = Math.min(e, m._getDaysInMonth(c, d));
                            break }
                    j = i.exec(a) }
                return new Date(c, d, e, f, g, h, 0) };
            var p = (k == null ? l : (typeof k == 'string' ? o(k) : (typeof k == 'number' ? n(k) : k)));
            if (p) p.setMilliseconds(0);
            return p }, _getDaysInMonth: function(a, b) {
            return 32 - new Date(a, b, 32).getDate() }, _normalLabels: function(a) {
            return a }, _generateHTML: function(c) {
            var d = this;
            c._periods = (c._hold ? c._periods : this._calculatePeriods(c, c._show, c.options.significant, new Date()));
            var e = false;
            var f = 0;
            var g = c.options.significant;
            var h = $.extend({}, c._show);
            for (var i = Y; i <= S; i++) { e |= (c._show[i] == '?' && c._periods[i] > 0);
                h[i] = (c._show[i] == '?' && !e ? null : c._show[i]);
                f += (h[i] ? 1 : 0);
                g -= (c._periods[i] > 0 ? 1 : 0) }
            var j = [false, false, false, false, false, false, false];
            for (var i = S; i >= Y; i--) {
                if (c._show[i]) {
                    if (c._periods[i]) { j[i] = true } else { j[i] = g > 0;
                        g-- } } }
            var k = (c.options.compact ? c.options.compactLabels : c.options.labels);
            var l = c.options.whichLabels || this._normalLabels;
            var m = function(a) {
                var b = c.options['compactLabels' + l(c._periods[a])];
                return (h[a] ? d._translateDigits(c, c._periods[a]) + (b ? b[a] : k[a]) + ' ' : '') };
            var n = (c.options.padZeroes ? 2 : 1);
            var o = function(a) {
                var b = c.options['labels' + l(c._periods[a])];
                return ((!c.options.significant && h[a]) || (c.options.significant && j[a]) ? '<span class="' + d._sectionClass + '">' + '<span class="' + d._amountClass + '">' + d._minDigits(c, c._periods[a], n) + '</span>' + '<span class="' + d._periodClass + '">' + (b ? b[a] : k[a]) + '</span></span>' : '') };
            return (c.options.layout ? this._buildLayout(c, h, c.options.layout, c.options.compact, c.options.significant, j) : ((c.options.compact ? '<span class="' + this._rowClass + ' ' + this._amountClass + (c._hold ? ' ' + this._holdingClass : '') + '">' + m(Y) + m(O) + m(W) + m(D) + (h[H] ? this._minDigits(c, c._periods[H], 2) : '') + (h[M] ? (h[H] ? c.options.timeSeparator : '') + this._minDigits(c, c._periods[M], 2) : '') + (h[S] ? (h[H] || h[M] ? c.options.timeSeparator : '') + this._minDigits(c, c._periods[S], 2) : '') : '<span class="' + this._rowClass + ' ' + this._showClass + (c.options.significant || f) + (c._hold ? ' ' + this._holdingClass : '') + '">' + o(Y) + o(O) + o(W) + o(D) + o(H) + o(M) + o(S)) + '</span>' + (c.options.description ? '<span class="' + this._rowClass + ' ' + this._descrClass + '">' + c.options.description + '</span>' : ''))) }, _buildLayout: function(c, d, e, f, g, h) {
            var j = c.options[f ? 'compactLabels' : 'labels'];
            var k = c.options.whichLabels || this._normalLabels;
            var l = function(a) {
                return (c.options[(f ? 'compactLabels' : 'labels') + k(c._periods[a])] || j)[a] };
            var m = function(a, b) {
                return c.options.digits[Math.floor(a / b) % 10] };
            var o = { desc: c.options.description, sep: c.options.timeSeparator, yl: l(Y), yn: this._minDigits(c, c._periods[Y], 1), ynn: this._minDigits(c, c._periods[Y], 2), ynnn: this._minDigits(c, c._periods[Y], 3), y1: m(c._periods[Y], 1), y10: m(c._periods[Y], 10), y100: m(c._periods[Y], 100), y1000: m(c._periods[Y], 1000), ol: l(O), on: this._minDigits(c, c._periods[O], 1), onn: this._minDigits(c, c._periods[O], 2), onnn: this._minDigits(c, c._periods[O], 3), o1: m(c._periods[O], 1), o10: m(c._periods[O], 10), o100: m(c._periods[O], 100), o1000: m(c._periods[O], 1000), wl: l(W), wn: this._minDigits(c, c._periods[W], 1), wnn: this._minDigits(c, c._periods[W], 2), wnnn: this._minDigits(c, c._periods[W], 3), w1: m(c._periods[W], 1), w10: m(c._periods[W], 10), w100: m(c._periods[W], 100), w1000: m(c._periods[W], 1000), dl: l(D), dn: this._minDigits(c, c._periods[D], 1), dnn: this._minDigits(c, c._periods[D], 2), dnnn: this._minDigits(c, c._periods[D], 3), d1: m(c._periods[D], 1), d10: m(c._periods[D], 10), d100: m(c._periods[D], 100), d1000: m(c._periods[D], 1000), hl: l(H), hn: this._minDigits(c, c._periods[H], 1), hnn: this._minDigits(c, c._periods[H], 2), hnnn: this._minDigits(c, c._periods[H], 3), h1: m(c._periods[H], 1), h10: m(c._periods[H], 10), h100: m(c._periods[H], 100), h1000: m(c._periods[H], 1000), ml: l(M), mn: this._minDigits(c, c._periods[M], 1), mnn: this._minDigits(c, c._periods[M], 2), mnnn: this._minDigits(c, c._periods[M], 3), m1: m(c._periods[M], 1), m10: m(c._periods[M], 10), m100: m(c._periods[M], 100), m1000: m(c._periods[M], 1000), sl: l(S), sn: this._minDigits(c, c._periods[S], 1), snn: this._minDigits(c, c._periods[S], 2), snnn: this._minDigits(c, c._periods[S], 3), s1: m(c._periods[S], 1), s10: m(c._periods[S], 10), s100: m(c._periods[S], 100), s1000: m(c._periods[S], 1000) };
            var p = e;
            for (var i = Y; i <= S; i++) {
                var q = 'yowdhms'.charAt(i);
                var r = new RegExp('\\{' + q + '<\\}([\\s\\S]*)\\{' + q + '>\\}', 'g');
                p = p.replace(r, ((!g && d[i]) || (g && h[i]) ? '$1' : '')) }
            $.each(o, function(n, v) {
                var a = new RegExp('\\{' + n + '\\}', 'g');
                p = p.replace(a, v) });
            return p }, _minDigits: function(a, b, c) { b = '' + b;
            if (b.length >= c) {
                return this._translateDigits(a, b) }
            b = '0000000000' + b;
            return this._translateDigits(a, b.substr(b.length - c)) }, _translateDigits: function(b, c) {
            return ('' + c).replace(/[0-9]/g, function(a) {
                return b.options.digits[a] }) }, _determineShow: function(a) {
            var b = a.options.format;
            var c = [];
            c[Y] = (b.match('y') ? '?' : (b.match('Y') ? '!' : null));
            c[O] = (b.match('o') ? '?' : (b.match('O') ? '!' : null));
            c[W] = (b.match('w') ? '?' : (b.match('W') ? '!' : null));
            c[D] = (b.match('d') ? '?' : (b.match('D') ? '!' : null));
            c[H] = (b.match('h') ? '?' : (b.match('H') ? '!' : null));
            c[M] = (b.match('m') ? '?' : (b.match('M') ? '!' : null));
            c[S] = (b.match('s') ? '?' : (b.match('S') ? '!' : null));
            return c }, _calculatePeriods: function(c, d, e, f) { c._now = f;
            c._now.setMilliseconds(0);
            var g = new Date(c._now.getTime());
            if (c._since) {
                if (f.getTime() < c._since.getTime()) { c._now = f = g } else { f = c._since } } else { g.setTime(c._until.getTime());
                if (f.getTime() > c._until.getTime()) { c._now = f = g } }
            var h = [0, 0, 0, 0, 0, 0, 0];
            if (d[Y] || d[O]) {
                var i = this._getDaysInMonth(f.getFullYear(), f.getMonth());
                var j = this._getDaysInMonth(g.getFullYear(), g.getMonth());
                var k = (g.getDate() == f.getDate() || (g.getDate() >= Math.min(i, j) && f.getDate() >= Math.min(i, j)));
                var l = function(a) {
                    return (a.getHours() * 60 + a.getMinutes()) * 60 + a.getSeconds() };
                var m = Math.max(0, (g.getFullYear() - f.getFullYear()) * 12 + g.getMonth() - f.getMonth() + ((g.getDate() < f.getDate() && !k) || (k && l(g) < l(f)) ? -1 : 0));
                h[Y] = (d[Y] ? Math.floor(m / 12) : 0);
                h[O] = (d[O] ? m - h[Y] * 12 : 0);
                f = new Date(f.getTime());
                var n = (f.getDate() == i);
                var o = this._getDaysInMonth(f.getFullYear() + h[Y], f.getMonth() + h[O]);
                if (f.getDate() > o) { f.setDate(o) }
                f.setFullYear(f.getFullYear() + h[Y]);
                f.setMonth(f.getMonth() + h[O]);
                if (n) { f.setDate(o) } }
            var p = Math.floor((g.getTime() - f.getTime()) / 1000);
            var q = function(a, b) { h[a] = (d[a] ? Math.floor(p / b) : 0);
                p -= h[a] * b };
            q(W, 604800);
            q(D, 86400);
            q(H, 3600);
            q(M, 60);
            q(S, 1);
            if (p > 0 && !c._since) {
                var r = [1, 12, 4.3482, 7, 24, 60, 60];
                var s = S;
                var t = 1;
                for (var u = S; u >= Y; u--) {
                    if (d[u]) {
                        if (h[s] >= t) { h[s] = 0;
                            p = 1 }
                        if (p > 0) { h[u]++;
                            p = 0;
                            s = u;
                            t = 1 } }
                    t *= r[u] } }
            if (e) {
                for (var u = Y; u <= S; u++) {
                    if (e && h[u]) { e-- } else if (!e) { h[u] = 0 } } }
            return h } }) })(jQuery);

$(document).ready(function () {
    $('a.scrollto').click(function () {

        $("html, body").animate({scrollTop: $("form").offset().top - 300}, 1000);

        event.preventDefault();

    });
    //
    var d = new Date();
    var elements = document.getElementsByClassName('today');
    var month = d.getMonth();
    if(month < 10) {
        month = '0'+month;
    }
    elements.map = [].map;
    elements.map(function (e) {
        e.innerHTML = d.getDate() + '.' + month + '.' + d.getFullYear();
    });
    //

    var _currentDate = new Date();
    var count = 30; // 8 hours
    var _toDate = new Date(_currentDate.getFullYear(), _currentDate.getMonth(), _currentDate.getDate(), _currentDate.getHours(), _currentDate.getMinutes() + count, 1);

    $('.timeqwqw').countdown({
        until: _toDate,
        format: 'HMS',
        compact: true,
        layout: /* Hours */
        '<span class="hour">{h10}{h1}</span>' +

        /* Monutes */
        '<span class="minute">{m10}{m1}</span>' +

        /* Seconds */
        '<span class="second">{s10}{s1}</span>' +

        '<div class="clear"></div>'
    }).removeClass('hidden');

    var scrollTop = localStorage.getItem('offsetTop');
    $(window).scrollTop(scrollTop);
    localStorage.setItem('offsetTop', 0);

});






// еверад 4.0, с фиксом через cdn_path, для проверки на сервере
//для клонирования блока в попап используются следующие айдишники
// #cloneThis - для десктопа
// #cloneMobileThis - для мобильного (если нужно)
//брейкпоинт для переключения попапа при необходимости дефолт значение = 1000

// в случае, если мы не клонируем форму, а верстаем попап произвольно,
// то делать это необходимо в контейнере с классом .ever-popup-build
// false (показывать контейнер) / true (не показывать контейнер)

var popupBuild = true; // false/true


//.ever-popup-btn - класс для для открытия попапа

//проверка кода
//.check__field - класс для поля проверки кода
//.check__btn - класс для кнопки провеки кода
//.check__result - класс для контейнера с результатом проверки кода

//таймер
//для вывода счетчика таймера используется 3 контенера (часы, минуты, секунды)
//.hours класс для вывода часов
//.minutes класс для вывода минут
//.seconds класс для вывода секунд

console.log(window.cdn_path);
if(!window.cdn_path){

    (function () {

        function initiate() {

            var breakpoint = 999,
                desktop = document.querySelector('#cloneThis'),
                mobile = document.querySelector('#cloneMobileThis');

            if (popupBuild) {
                // в случае, если мы верстаем попап в контейнере .ever-popup-build, даное условие прячет его, если значение переменной popupBuild = true
                var style = document.createElement('style');
                style.innerHTML = '.ever-popup-build{position: fixed; opacity: 0;z-index: -1; top: 0; left: -9999px;}';
                document.querySelector('head').appendChild(style)
            }

            function addPopupStyle() {
                // добавляем стили для нашего поапа
                var cont = document.createElement('style'),
                    head = document.querySelector('head');
                cont.innerHTML = '.ever-popup__body.ever-mobile{display:none}.ever-popup{position: fixed;top: 0;left: 0;width: 100%;height: 100%;background: rgba(0,0,0,.7);z-index: 111;display: none;overflow: auto;}.ever-popup__body{position: static;float: none;display: block;margin: 0 auto;width:auto}.ever-popup.show{display: block;align-items: center;}.ever-popup__inner{position: relative;margin: 0 auto;padding-top:35px}.ever-popup__close{width: 35px;height: 30px;position: absolute;cursor:pointer;top: 0;right: 0;z-index: 1;-webkit-transition: .3s; -moz-transition: .3s; -ms-transition: .3s; -o-transition: .3s; transition: .3s;}.ever-popup__close:after, .ever-popup__close:before {content: "";position: absolute;right: 0;top: 10px;width: 35px;height: 10px;background: #fff;transition: all 1s;}.ever-popup__close:after {-webkit-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);}.ever-popup__close:before {-webkit-transform: rotate(45deg);-ms-transform: rotate(45deg);-o-transform: rotate(45deg);transform: rotate(45deg);}' +
                    '@media screen and (max-width: ' + breakpoint + 'px' + '){' +
                    '.ever-popup__body.ever-desktop{display:none}' +
                    '.ever-popup__body.ever-mobile{display:block}' +
                    '}';
                head.appendChild(cont)
            }

            function addMobilePopupStyle() {
                // добавляем стили для нашего поапа
                var cont = document.createElement('style'),
                    head = document.querySelector('head');
                cont.innerHTML = '@media screen and (max-width: ' + breakpoint + 'px' + ') {.ever-popup {position: fixed;top: 0;left: 0;width: 100%;height: 100%;background: rgba(0, 0, 0, .7);z-index: 111;display: none;overflow: auto;}.ever-popup__body {position: static;float: none;display: block;margin: 0 auto;width: auto}.ever-popup.show {display: block;align-items: center;}.ever-popup__inner {position: relative;margin: 0 auto;padding-top: 35px}.ever-popup__close {width: 35px;height: 30px;position: absolute;cursor: pointer;top: 0;right: 0;z-index: 1;-webkit-transition: .3s;-moz-transition: .3s;-ms-transition: .3s;-o-transition: .3s;transition: .3s;}.ever-popup__close:after, .ever-popup__close:before {content: "";position: absolute;right: 0;top: 10px;width: 35px;height: 10px;background: #fff;transition: all 1s;}.ever-popup__close:after {-webkit-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);}.ever-popup__close:before {-webkit-transform: rotate(45deg);-ms-transform: rotate(45deg);-o-transform: rotate(45deg);transform: rotate(45deg);}}';
                head.appendChild(cont)
            }

            function createOverlay() {
                // создаем затемненный фон для попапа и вставляем его в разметку html
                var parent = document.createElement('div'),
                    parentInner = document.createElement('div'),
                    closeParent = document.createElement('div');

                parent.classList.add('ever-popup');
                parentInner.classList.add('ever-popup__inner');
                closeParent.classList.add('ever-popup__close');

                parent.appendChild(parentInner);
                parentInner.appendChild(closeParent);
                document.body.appendChild(parent);
            }

            function createModalBody(breakpoint) {
                // функция определяет содержимое для попапа, клонирует его содержимое, и поещает в контейнер ever-popup__body
                var parent = document.querySelector('.ever-popup__inner');

                if (desktop) {
                    var desktopClone = desktop.cloneNode(true);
                    desktopClone.classList.add('ever-popup__body');
                    desktopClone.removeAttribute('id');
                    parent.appendChild(desktopClone);
                    document.querySelector('.ever-popup .ever-popup__inner').style.width = document.querySelector('#cloneThis').offsetWidth + 'px';
                }

                if (mobile) {
                    var mobileClone = mobile.cloneNode(true);
                    if (desktopClone) {
                        desktopClone.classList.add('ever-desktop');
                    }
                    mobileClone.classList.add('ever-popup__body');
                    mobileClone.classList.add('ever-mobile');
                    mobileClone.removeAttribute('id');
                    parent.appendChild(mobileClone);
                    var mobileStyles = '.ever-desktop{display: block}.ever-mobile{display: none}@media screen and (max-width: ' + breakpoint + 'px){.ever-mobile{display: block}.ever-desktop{display: none;}}';

                    var mobileStylesContainer = document.createElement('style');
                    mobileStylesContainer.innerHTML = mobileStyles;
                    document.querySelector('head').appendChild(mobileStylesContainer)
                    document.querySelector('.ever-popup .ever-popup__inner').style.width = document.querySelector('#cloneMobileThis').offsetWidth + 'px';
                    console.log(mobile.offsetWidth)
                }
            }

            function modalPosition(screenHeight) {
                //расчет ширины и вывод ее в html, функция вызывается при загрузке страницы, а так же при ресайзе
                var container = document.querySelector('.ever-popup  .ever-popup__inner');
                if (container) {

                    var desktop = document.querySelector('#cloneThis'),
                        mobile = document.querySelector('#cloneMobileThis');


                    if (desktop) {
                        checkPosition(desktop, container, screenHeight);
                        if (window.innerWidth >= breakpoint) {
                            container.style.width = desktop.offsetWidth + 'px';
                        }
                        if(!mobile){
                            container.style.width = desktop.offsetWidth + 'px';
                        }
                    }
                    if (mobile) {
                        checkPosition(mobile, container, screenHeight);
                        if (window.innerWidth <= breakpoint) {
                            container.style.width = mobile.offsetWidth + 'px';
                        }
                    }
                }
            }

            function checkPosition(selector, container, screenHeight) {
                //позиционирование попапа по вертикали

                var cont = selector,
                    contHeight = cont.offsetHeight;

                if (contHeight > screenHeight) {
                    container.style.margin = '40px auto';
                }
                else {
                    var top = (screenHeight - contHeight) / 2;
                    container.style.margin = top + 'px auto 20px';
                }
            }

            function showPopup() {
                //функция для показа попапа
                var popup = document.querySelector('.ever-popup');
                popup.classList.add('show')
            }

            function hidePopup() {
                //функция для скрытия попапа
                var popup = document.querySelector('.ever-popup');
                popup.classList.remove('show')
            }

            function notHide(e) {
                //функция для прерывания выполнения сценария по клику
                e.stopPropagation()
            }

            function checkCode(event) {
                // проверка кода подлинности
                event.preventDefault();

                var code = document.querySelector(".check__field").value,
                    msg = document.querySelector(".check__result");

                if (code.length === 15) {
                    msg.innerHTML = 'Данный код верен. Спасибо, что выбрали нашу продукцию!';
                }
                else if (code.length === 0) {
                    msg.innerHTML = 'Введите, пожалуйста, код.';
                } else {
                    msg.innerHTML = 'К сожалению, данный код не найден! Вероятнее всего, вы приобрели поддельный продукт.';
                }
            }

            var mouseOutCount = 0;
            document.body.addEventListener('mouseleave', function (event) {
                //событие на увод мышки со страницы. если мышка уходит за верхнюю границу документа, вызывается попап
                var e = event || window.event;
                e = e.clientY;
                var popup = document.querySelector('.ever-popup');

                if (popup && e < 10 && mouseOutCount === 0) {
                    popup.classList.add('show');
                    mouseOutCount++;
                }
            });

            function addPhoneBtn(breakpoint) {
                // добавление синей трубки для вызова попапа на десктопе
                var phoneBtnContainer = document.createElement('div');
                phoneBtnContainer.classList.add('phoneBtnContainer');
                phoneBtnContainer.innerHTML = '';
                document.body.appendChild(phoneBtnContainer);

                var phoneStyles = document.createElement('style');
                phoneStyles.innerHTML = '.phoneBtnContainer{position:fixed; right: 10px;bottom: 10px; visibility:hidden;background-color:transparent;width:200px;height:200px;cursor:pointer;z-index:99;-webkit-backface-visibility:hidden;-webkit-transform:translateZ(0);-webkit-transition:visibility .5s;-moz-transition:visibility .5s;-o-transition:visibility .5s;transition:visibility .5s}.cbh-phone.cbh-show{visibility:visible}@-webkit-keyframes fadeInRight{0%{opacity:0;-webkit-transform:translate3d(100%,0,0);transform:translate3d(100%,0,0)}100%{opacity:1;-webkit-transform:none;transform:none}}@keyframes fadeInRight{0%{opacity:0;-webkit-transform:translate3d(100%,0,0);-ms-transform:translate3d(100%,0,0);transform:translate3d(100%,0,0)}100%{opacity:1;-webkit-transform:none;-ms-transform:none;transform:none}}@-webkit-keyframes fadeInRightBig{0%{opacity:0;-webkit-transform:translate3d(2000px,0,0);transform:translate3d(2000px,0,0)}100%{opacity:1;-webkit-transform:none;transform:none}}@-webkit-keyframes fadeOutRight{0%{opacity:1}100%{opacity:0;-webkit-transform:translate3d(100%,0,0);transform:translate3d(100%,0,0)}}@keyframes fadeOutRight{0%{opacity:1}100%{opacity:0;-webkit-transform:translate3d(100%,0,0);-ms-transform:translate3d(100%,0,0);transform:translate3d(100%,0,0)}}.fadeOutRight{-webkit-animation-name:fadeOutRight;animation-name:fadeOutRight}.cbh-phone.cbh-static1{opacity:.6}.cbh-phone.cbh-hover1{opacity:1}.cbh-ph-circle{width:160px;height:160px;top:20px;left:20px;position:absolute;background-color:transparent;-webkit-border-radius:100%;-moz-border-radius:100%;border-radius:100%;border:2px solid rgba(30,30,30,.4);opacity:.1;-webkit-animation:cbh-circle-anim 1.2s infinite ease-in-out;-moz-animation:cbh-circle-anim 1.2s infinite ease-in-out;-ms-animation:cbh-circle-anim 1.2s infinite ease-in-out;-o-animation:cbh-circle-anim 1.2s infinite ease-in-out;animation:cbh-circle-anim 1.2s infinite ease-in-out;-webkit-transition:all .5s;-moz-transition:all .5s;-o-transition:all .5s;transition:all .5s}.cbh-phone.cbh-active .cbh-ph-circle1{-webkit-animation:cbh-circle-anim 1.1s infinite ease-in-out!important;-moz-animation:cbh-circle-anim 1.1s infinite ease-in-out!important;-ms-animation:cbh-circle-anim 1.1s infinite ease-in-out!important;-o-animation:cbh-circle-anim 1.1s infinite ease-in-out!important;animation:cbh-circle-anim 1.1s infinite ease-in-out!important}.cbh-phone.cbh-static .cbh-ph-circle{-webkit-animation:cbh-circle-anim 2.2s infinite ease-in-out!important;-moz-animation:cbh-circle-anim 2.2s infinite ease-in-out!important;-ms-animation:cbh-circle-anim 2.2s infinite ease-in-out!important;-o-animation:cbh-circle-anim 2.2s infinite ease-in-out!important;animation:cbh-circle-anim 2.2s infinite ease-in-out!important}.cbh-phone.cbh-hover .cbh-ph-circle{border-color:rgba(0,175,242,1);opacity:.5}.cbh-phone.cbh-green.cbh-hover .cbh-ph-circle{border-color:rgba(117,235,80,1);opacity:.5}.cbh-phone.cbh-green .cbh-ph-circle{border-color:rgba(0,175,242,1);opacity:.5}.cbh-phone.cbh-gray.cbh-hover .cbh-ph-circle{border-color:rgba(204,204,204,1);opacity:.5}.cbh-phone.cbh-gray .cbh-ph-circle{border-color:rgba(117,235,80,1);opacity:.5}.cbh-ph-circle-fill{width:100px;height:100px;top:50px;left:50px;position:absolute;background-color:#000;-webkit-border-radius:100%;-moz-border-radius:100%;border-radius:100%;border:2px solid transparent;opacity:.1;-webkit-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out;-moz-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out;-ms-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out;-o-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out;animation:cbh-circle-fill-anim 2.3s infinite ease-in-out;-webkit-transition:all .5s;-moz-transition:all .5s;-o-transition:all .5s;transition:all .5s}.cbh-phone.cbh-active .cbh-ph-circle-fill{-webkit-animation:cbh-circle-fill-anim 1.7s infinite ease-in-out!important;-moz-animation:cbh-circle-fill-anim 1.7s infinite ease-in-out!important;-ms-animation:cbh-circle-fill-anim 1.7s infinite ease-in-out!important;-o-animation:cbh-circle-fill-anim 1.7s infinite ease-in-out!important;animation:cbh-circle-fill-anim 1.7s infinite ease-in-out!important}.cbh-phone.cbh-static .cbh-ph-circle-fill{-webkit-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out!important;-moz-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out!important;-ms-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out!important;-o-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out!important;animation:cbh-circle-fill-anim 2.3s infinite ease-in-out!important;opacity:0!important} .cbh-phone.cbh-hover .cbh-ph-circle-fill{background-color:rgba(0,175,242,.5);opacity:.75!important}.cbh-phone.cbh-green.cbh-hover .cbh-ph-circle-fill{background-color:rgba(117,235,80,.5);opacity:.75!important}.cbh-phone.cbh-green .cbh-ph-circle-fill{background-color:rgba(0,175,242,.5);opacity:.75!important}.cbh-phone.cbh-gray.cbh-hover .cbh-ph-circle-fill{background-color:rgba(204,204,204,.5);opacity:.75!important}.cbh-phone.cbh-gray .cbh-ph-circle-fill{background-color:rgba(117,235,80,.5);opacity:.75!important}.cbh-ph-img-circle1{width:60px;height:60px;top:70px;left:70px;position:absolute;background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAB/ElEQVR42uya7W3CMBCG31QM4A1aNggTlG6QbpBMkHYC1AloJ4BOABuEDcgGtBOETnD9c1ERCH/lwxeaV8oPFGP86Hy+DxMREW5Bd7gRjSDSNGn4/RiAOvm8C0ZCRD5PSkQVXSr1nK/xE3mcWimA1ZV3JYBZCIO4giQANoYxMwYS6+xKY4lT5dJPreWZY+uspqSCKPYN27GJVBDXheVSQe494ksiEWTuMXcu1dld9SARxDX1OAJ4lgjy4zDnFsC076A4adEiRwAZg4hOUSpNoCsBPDGM+HqkNGynYBCuILuWj+dgWysGsNe8nwL4GsrW0m2fxZBq9rW0rNcX5MOQ9eZD8JFahcG5g/iKT671alGAYQggpYWvpEPYWrU/HDTOfeRIX0q2SL3QN4tGhZJukVobQyXYWw7WtLDKDIuM+ZSzscyCE9PCy5IttCvnZNaeiGLNHKuz8ZVh/MXTVu/1xQKmIqLEAuJ0fNo3iG5B51oSkeKnsBi/4bG9gYB/lCytU5G9DryFW+3Gm+JLwU7ehbJrwTjq4DJU8bHcVbEV9dXXqqP6uqO5e2/QZRYJpqu2IUAA4B3tXvx8hgKp05QZW6dJqrLTNkB6vrRURLRwPHqtYgkC3cLWQAcDQGGKH13FER/NATzi786+BPDNjm1dMkfjn2pGkBHkf4D8DgBJDuDHx9BN+gAAAABJRU5ErkJggg==);background-color:rgba(30,30,30,.1);background-position:center center;background-repeat:no-repeat;-webkit-border-radius:100%;-moz-border-radius:100%;border-radius:100%;border:2px solid transparent;opacity:.7;-webkit-animation:cbh-circle-img-anim 1s infinite ease-in-out;-moz-animation:cbh-circle-img-anim 1s infinite ease-in-out;-ms-animation:cbh-circle-img-anim 1s infinite ease-in-out;-o-animation:cbh-circle-img-anim 1s infinite ease-in-out;animation:cbh-circle-img-anim 1s infinite ease-in-out}.cbh-phone.cbh-active .cbh-ph-img-circle1{-webkit-animation:cbh-circle-img-anim 1s infinite ease-in-out!important;-moz-animation:cbh-circle-img-anim 1s infinite ease-in-out!important;-ms-animation:cbh-circle-img-anim 1s infinite ease-in-out!important;-o-animation:cbh-circle-img-anim 1s infinite ease-in-out!important;animation:cbh-circle-img-anim 1s infinite ease-in-out!important}.cbh-phone.cbh-static .cbh-ph-img-circle1{-webkit-animation:cbh-circle-img-anim 0s infinite ease-in-out!important;-moz-animation:cbh-circle-img-anim 0s infinite ease-in-out!important;-ms-animation:cbh-circle-img-anim 0s infinite ease-in-out!important;-o-animation:cbh-circle-img-anim 0s infinite ease-in-out!important;animation:cbh-circle-img-anim 0s infinite ease-in-out!important}.cbh-phone.cbh-hover .cbh-ph-img-circle1{background-color:rgba(0,175,242,1)}.cbh-phone.cbh-green.cbh-hover .cbh-ph-img-circle1:hover{background-color:rgba(117,235,80,1)}.cbh-phone.cbh-green .cbh-ph-img-circle1{background-color:rgba(0,175,242,1)}.cbh-phone.cbh-green .cbh-ph-img-circle1{background-color:rgba(0,175,242,1)}.cbh-phone.cbh-gray.cbh-hover .cbh-ph-img-circle1{background-color:rgba(204,204,204,1)}.cbh-phone.cbh-gray .cbh-ph-img-circle1{background-color:rgba(117,235,80,1)}@-moz-keyframes cbh-circle-anim{0%{-moz-transform:rotate(0deg) scale(0.5) skew(1deg);opacity:.1;-moz-opacity:.1;-webkit-opacity:.1;-o-opacity:.1}30%{-moz-transform:rotate(0deg) scale(.7) skew(1deg);opacity:.5;-moz-opacity:.5;-webkit-opacity:.5;-o-opacity:.5}100%{-moz-transform:rotate(0deg) scale(1) skew(1deg);opacity:.6;-moz-opacity:.6;-webkit-opacity:.6;-o-opacity:.1}}@-webkit-keyframes cbh-circle-anim{0%{-webkit-transform:rotate(0deg) scale(0.5) skew(1deg);-webkit-opacity:.1}30%{-webkit-transform:rotate(0deg) scale(.7) skew(1deg);-webkit-opacity:.5}100%{-webkit-transform:rotate(0deg) scale(1) skew(1deg);-webkit-opacity:.1}}@-o-keyframes cbh-circle-anim{0%{-o-transform:rotate(0deg) kscale(0.5) skew(1deg);-o-opacity:.1}30%{-o-transform:rotate(0deg) scale(.7) skew(1deg);-o-opacity:.5}100%{-o-transform:rotate(0deg) scale(1) skew(1deg);-o-opacity:.1}}@keyframes cbh-circle-anim{0%{transform:rotate(0deg) scale(0.5) skew(1deg);opacity:.1}30%{transform:rotate(0deg) scale(.7) skew(1deg);opacity:.5}100%{transform:rotate(0deg) scale(1) skew(1deg);opacity:.1}}@-moz-keyframes cbh-circle-fill-anim{0%{-moz-transform:rotate(0deg) scale(0.7) skew(1deg);opacity:.2}50%{-moz-transform:rotate(0deg) -moz-scale(1) skew(1deg);opacity:.2}100%{-moz-transform:rotate(0deg) scale(0.7) skew(1deg);opacity:.2}}@-webkit-keyframes cbh-circle-fill-anim{0%{-webkit-transform:rotate(0deg) scale(0.7) skew(1deg);opacity:.2}50%{-webkit-transform:rotate(0deg) scale(1) skew(1deg);opacity:.2}100%{-webkit-transform:rotate(0deg) scale(0.7) skew(1deg);opacity:.2}}@-o-keyframes cbh-circle-fill-anim{0%{-o-transform:rotate(0deg) scale(0.7) skew(1deg);opacity:.2}50%{-o-transform:rotate(0deg) scale(1) skew(1deg);opacity:.2}100%{-o-transform:rotate(0deg) scale(0.7) skew(1deg);opacity:.2}}@keyframes cbh-circle-fill-anim{0%{transform:rotate(0deg) scale(0.7) skew(1deg);opacity:.2}50%{transform:rotate(0deg) scale(1) skew(1deg);opacity:.2}100%{transform:rotate(0deg) scale(0.7) skew(1deg);opacity:.2}}@keyframes cbh-circle-img-anim{0%{transform:rotate(0deg) scale(1) skew(1deg)}10%{transform:rotate(-25deg) scale(1) skew(1deg)}20%{transform:rotate(25deg) scale(1) skew(1deg)}30%{transform:rotate(-25deg) scale(1) skew(1deg)}40%{transform:rotate(25deg) scale(1) skew(1deg)}100%,50%{transform:rotate(0deg) scale(1) skew(1deg)}}@-moz-keyframes cbh-circle-img-anim{0%{transform:rotate(0deg) scale(1) skew(1deg)}10%{-moz-transform:rotate(-25deg) scale(1) skew(1deg)}20%{-moz-transform:rotate(25deg) scale(1) skew(1deg)}30%{-moz-transform:rotate(-25deg) scale(1) skew(1deg)}40%{-moz-transform:rotate(25deg) scale(1) skew(1deg)}100%,50%{-moz-transform:rotate(0deg) scale(1) skew(1deg)}}@-webkit-keyframes cbh-circle-img-anim{0%{-webkit-transform:rotate(0deg) scale(1) skew(1deg)}10%{-webkit-transform:rotate(-25deg) scale(1) skew(1deg)}20%{-webkit-transform:rotate(25deg) scale(1) skew(1deg)}30%{-webkit-transform:rotate(-25deg) scale(1) skew(1deg)}40%{-webkit-transform:rotate(25deg) scale(1) skew(1deg)}100%,50%{-webkit-transform:rotate(0deg) scale(1) skew(1deg)}}@-o-keyframes cbh-circle-img-anim{0%{-o-transform:rotate(0deg) scale(1) skew(1deg)}10%{-o-transform:rotate(-25deg) scale(1) skew(1deg)}20%{-o-transform:rotate(25deg) scale(1) skew(1deg)}30%{-o-transform:rotate(-25deg) scale(1) skew(1deg)}40%{-o-transform:rotate(25deg) scale(1) skew(1deg)}100%,50%{-o-transform:rotate(0deg) scale(1) skew(1deg)}}.cbh-ph-img-circle1 {}.cbh-phone.cbh-green .cbh-ph-circle {border-color: rgba(0, 175, 242, 1)}.cbh-phone.cbh-green .cbh-ph-circle-fill {background-color: rgba(0, 175, 242, 1);}.cbh-phone.cbh-green .cbh-ph-img-circle1 {background-color:rgba(0, 175, 242, 1);}body, div, dl, dt, dd, ul, ol, li, nav, h1, h2, h3, h4, h5, h6, pre, code, form, fieldset, legend, input, button, textarea, p, blockquote, th, td, a {-webkit-transform-origin: center center;-ms-transform-origin: center center;-o-transform-origin: center center;transform-origin: center center;}@media screen and (max-width: ' + breakpoint + 'px) {#clbh_phone_div{display: none !important;}}';
                document.querySelector('head').appendChild(phoneStyles)
            }

            function init() {

                var desktopPopup = document.querySelector('#cloneThis'),
                    mobilePopup = document.querySelector('#cloneMobileThis');

                var h = document.querySelector('.hours'), m = document.querySelector('.minutes'),
                    s = document.querySelector('.seconds');

                if (h && m && s) {
                    // если все значения (часы/минуты/секунды) сущесвтуют, тогда срабатывает таймер
                    initializeTimer();
                }
                if (desktopPopup) {
                    createOverlay();
                    addPopupStyle();
                    addPhoneBtn(breakpoint);
                    document.querySelector('.phoneBtnContainer').addEventListener('click', showPopup);
                }
                else {
                    createOverlay();
                    addMobilePopupStyle()
                }
                if (desktopPopup || mobilePopup) {
                    //если у нас есть #cloneThis или #cloneMobileThis, тогда все функции ниже выполняются

                    createModalBody(breakpoint);
                    modalPosition(window.innerHeight);
                    document.querySelector('.ever-popup__close').addEventListener('click', hidePopup);
                    document.querySelector('.ever-popup__inner').addEventListener('click', notHide);
                    document.querySelector('.ever-popup').addEventListener('click', hidePopup);

                    var modalBtn = document.querySelectorAll('.ever-popup-btn, #kmacb a');
                    for (var i = 0; i < modalBtn.length; i++) {
                        modalBtn && modalBtn[i].addEventListener('click', showPopup);
                    }
                }
                // рабоатет если у нас есть класс .check__btn
                var checkBtn = document.querySelector(".check__btn");
                checkBtn && checkBtn.addEventListener('click', checkCode);
            }

            // при документ реди вызывается функция init, описаная выше
            document.addEventListener('DOMContentLoaded', init);

            window.addEventListener('resize', function () {
                //при ресайзе пересчитываем позиционирование модального окна
                modalPosition(window.innerHeight);
            });

            function initializeTimer() {

                if (!localStorage.getItem("heytimer")) {
                    var time = {
                        hours: 0,
                        minutes: 27,
                        seconds: 0
                    }, different = false;

                    time = time.hours * 3600 + time.minutes * 60 + time.seconds;

                    localStorage.setItem("time", time);
                    localStorage.setItem("heytimer", true);
                    localStorage.setItem("different", different);
                }

                timerSettings();
            }

            function timerSettings() {
                var time = localStorage.getItem('time'),
                    different = localStorage.getItem('different') === "true",
                    hours = parseInt(time / 3600, 10),
                    minutes = parseInt((time - hours * 3600 ) / 60, 10),
                    seconds = parseInt(time % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : "" + minutes;
                seconds = seconds < 10 ? "0" + seconds : "" + seconds;
                hours = hours < 10 ? "0" + hours : "" + hours;

                var hoursHTML = document.getElementsByClassName("hours");
                var minutesHTML = document.getElementsByClassName("minutes");
                var secondsHTML = document.getElementsByClassName("seconds");

                if (--time < 0) {
                    localStorage.removeItem("heytimer");
                    return;
                }
                if (different) {
                    seconds = seconds.split("");
                    minutes = minutes.split("");
                    hours = hours.split("");

                    doubleFilling(hoursHTML, hours);
                    doubleFilling(minutesHTML, minutes);
                    doubleFilling(secondsHTML, seconds);
                } else {
                    filling(hoursHTML, hours);
                    filling(minutesHTML, minutes);
                    filling(secondsHTML, seconds);
                }

                localStorage.setItem("time", time);
                setTimeout(timerSettings, 1000);
            }

            function filling(obj, value) {
                for (var i = 0; i < obj.length; i++) {
                    obj[i].innerHTML = value;
                }
            }

            function doubleFilling(obj, value) {
                for (var i = 0; i < obj.length; i++) {
                    obj[i].innerHTML = value[i % 2];
                }
            }
        }

        initiate();

    })();
}

